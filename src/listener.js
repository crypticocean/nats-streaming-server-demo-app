const nats = require("node-nats-streaming");
const { randomBytes } = require("crypto");

const { TicketCreatedListener } = require("./events/ticket-created-listener");

// clear previous logs from terminal
console.clear();

// randomly generate clientID
const stan = nats.connect("ticketing", randomBytes(4).toString("hex"), {
    url: "http://localhost:4222"
});

stan.on("connect", () => {
    console.log("Listener connected to NATS!");

    stan.on("close", () => {
        console.log("NATS connection closed!");
        process.exit();
    });

    new TicketCreatedListener(stan).listen();
});

// stan.on("connect", () => {
//     console.log("Listener connected to NATS!");

//     stan.on("close", () => {
//         console.log("NATS connection closed!");
//         process.exit();
//     });

//     /**
//      *      * stan.subscriptionOptions().setManualAckMode()
//      * Configures the subscription to require manual acknowledgement of messages
//      * using Message#acknowledge.
//      * @param tf - true if manual acknowlegement is required.
//      *
//      *      * stan.subscriptionOptions().setDeliverAllAvailable()
//      * Configures the subscription to replay from first available message.
//      *
//      *      *  stan.subscriptionOptions().setDurableName()
//      * Sets a durable subscription name that the client can specify for the subscription.
//      * This enables the subscriber to close the connection without canceling the subscription and
//      * resume the subscription with same durable name. Note the server will resume the
//      * subscription with messages
//      * that have not been acknowledged.
//      */
//     const options = stan
//         .subscriptionOptions()
//         .setManualAckMode(true)
//         .setDeliverAllAvailable()
//         .setDurableName("listener-service");

//     // subscribing to the queue group "listener-queue-group"
//     const subscription = stan.subscribe("ticket:created", "listener-queue-group", options);

//     subscription.on("message", (msg) => {
//         console.log("Message received!");

//         const data = msg.getData();

//         if(typeof data === "string") {
//             console.log(`PARSED DATA: \n Received event: \n SUBJECT: ${msg.getSubject()},\n SEQUENCE: #${msg.getSequence()},\n DATA: ${JSON.parse(data)}`);
//             console.log(`STRINGIFY DATA: \n Received event: \n SUBJECT: ${msg.getSubject()},\n SEQUENCE: #${msg.getSequence()},\n DATA: ${data}`);
//             console.log(`\n`);
//         }

//         /**
//          * Use msg.ack() to acknowledge the event
//          * If we do not acknowledge the event, the event will be send again after 30 seconds
//          */
//         msg.ack();
//     });
// });

// Handling closing (SIGTERM) or restarting (SIGINT) process (in terminal)
process.on("SIGINT", () => { stan.close() });
process.on("SIGTERM", () => { stan.close() });

/**
 * getSubject()
 * Returns name of the channel (subject).
 */

/**
 * getSequence()
 * Returns the sequence number of the message in the stream.
 */

/**
 * getData()
 * Returns the data associated with the message payload.
 * Need to run JSON.parse()
 */