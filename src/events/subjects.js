const subjects = {
    TicketCreated: "ticket:created",
}

module.exports = {
    subjects: Object.freeze(subjects)
};