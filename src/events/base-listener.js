class Listener {
    // abstract subject;
    // abstract queueGroupName;
    // abstract onMessage(data, msg)
    // private client;
    // private ackWait = 5 * 1000; // 5 sec

    ackWait = 5 * 1000; // 5 sec

    constructor(client) {
        this.client = client;
    }

    subscriptionOptions() {
        return this.client
            .subscriptionOptions()
            .setDeliverAllAvailable()
            .setManualAckMode(true)
            .setAckWait(this.ackWait)
            .setDurableName(this.queueGroupName);
    }

    listen() {
        const subscription = this.client.subscribe(
            this.subject,
            this.queueGroupName,
            this.subscriptionOptions(),
        );

        subscription.on("message", (msg) => {
            console.log(`Message received: ${this.subject} / ${this.queueGroupName}`);

            const parsedData = this.parseMessage(msg);
            this.onMessage(parsedData, msg);
        });
    }

    parseMessage(msg) {
        const data = msg.getData();

        if(typeof data === "string") {
            console.log(`PARSED DATA: \n Received event: \n SUBJECT: ${msg.getSubject()},\n SEQUENCE: #${msg.getSequence()},\n DATA: ${JSON.parse(data)}`);
            console.log(`STRINGIFY DATA: \n Received event: \n SUBJECT: ${msg.getSubject()},\n SEQUENCE: #${msg.getSequence()},\n DATA: ${data}`);
            console.log(`\n`);
        }

        return typeof data === "string"
            ?
                JSON.parse(data)
            :
                JSON.parse(data.toString("utf-8"));
    }
}

module.exports = {Listener};