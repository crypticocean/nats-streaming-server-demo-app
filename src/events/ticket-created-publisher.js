const { Publisher } = require("./base-publisher");
const { subjects } = require("./subjects");

class TicketCreatedPublisher extends Publisher {
    subject = subjects.TicketCreated;
}

module.exports = { TicketCreatedPublisher };