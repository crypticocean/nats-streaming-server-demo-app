const { Listener } = require("./base-listener");
const { subjects } = require("./subjects");

class TicketCreatedListener extends Listener {
    subject = subjects.TicketCreated;
    queueGroupName = "payments-service";
    onMessage(data, msg) {
        console.log("Event data: ", data);

        msg.ack();
    }
}

module.exports = { TicketCreatedListener };