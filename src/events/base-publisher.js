class Publisher {
    constructor(client) {
        this.client = client;
    }

    publish(data) {
        return new Promise((resolve, reject) => {
            this.client.publish(this.subject, JSON.stringify(data), (err) => {
                if (err) {
                    return reject(err);
                }
                console.log("Event published!");
                console.log("Event Data: ", data);

                resolve();
            });
        });

    }

    // publish(data) {
    //     this.client.publish(this.subject, JSON.stringify(data), () => {
    //         console.log("Event published!");
    //         console.log("Event Data: ", data);
    //     });
    // }
}

module.exports = { Publisher };