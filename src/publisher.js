const nats = require('node-nats-streaming');

const { TicketCreatedPublisher } = require("./events/ticket-created-publisher");

// clear previous logs from terminal
console.clear();

const stan = nats.connect("ticketing", "abc", {
    url: "http://localhost:4222"
});

stan.on("connect", async () => {
    console.log("Publisher connected to NODE NATS!");

    const publisher = new TicketCreatedPublisher(stan);

    try {
        await publisher.publish({
            id: "randomTicketID",
            title: "concert",
            price: 20,
        });
    } catch(err) {
        console.error("Error while publishing: ", err);
    }
});